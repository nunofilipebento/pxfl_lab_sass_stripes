//====================
document.addEventListener('DOMContentLoaded', onLoadDOM ) 

function onLoadDOM ()
{
	//......
	var btns = document.querySelectorAll(".btn_loader"); 

	//......
	[].forEach.call(btns, function (el) 
	{
		el.addEventListener('click', onClick, false);
	});
	//......
	function onClick (e)
	{
		e.preventDefault();
		//......
		var loader_sass, parent, elem;
		
		//......true | false (Init animation for each sass loader)
 		var active = this.getAttribute('data-active');
		
		//......which loader
 		loader_sass = this.getAttribute('data-loader');
		
		//......get parent .holder_loader
		parent = this.parentElement; 

		//......
		elem = parent.querySelector(".holder_loader1"); 
		
		//......
		if(active == "true")
		{	
			this.setAttribute('data-active', 'false');
			if (elem.classList) { elem.classList.remove(loader_sass); }

		}else{
			
			this.setAttribute('data-active', 'true');
			if (elem.classList) { elem.classList.add(loader_sass); }
		}		
	}//......onClick END!	
	//....................
}		
